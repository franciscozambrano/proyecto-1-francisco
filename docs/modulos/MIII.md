---
hide:
    - toc
---

# MI 03 - Innovacion Abierta

Se define como una nueva estrategia de innovación mediante la cual las empresas van más allá de sus límites y desarrollan la cooperación con organizaciones o profesionales externos (**[Henry
Chesbrough](https://en.wikipedia.org/wiki/Henry_Chesbrough)**)

![](../images/MI03/1.png){ width=50% }

![](../images/MI03/2.png){ width=50% }

**Reglas a tomar en cuenta para la innovación abierta**

● El problema o la pregunta que nos ocupa
● Los incentivos disponibles
● Expectativas de propiedad intelectual
● Cómo y cuándo las personas deben enviar ideas

**Modelos comunes de innovación abierta**

Desafíos
Asociaciones de empresas emergentes
Incubadora / aceleradora de inicio
Hackatones
Laboratorios de Co-creación

**Ejemplos**

**[Lego Ideas](https://ideas.lego.com/#all)**
**[Connet and Develop de P&G](https://www.pgconnectdevelop.com/)**
**[Portal de Inovacion de Unilever](https://www.unilever.com/about/innovation/open-innovation/)**

## Open Source

 “código abierto” se refiere al software cuyo código fuente se ha
puesto a disposición de todo el mundo de manera gratuita y otorgado con licencias
que facilita su reutilización o adaptación a contextos diferentes.
Este concepto promueve el intercambio de ideas
y valoriza la colaboración para mejorar el código de otros.

**Open Source no es lo mismo que software libre**

**Algunos Ejemplos**

![](../images/MI03/3.png){ width=50% }

**Open Hardware**
Similar al software con algunas particularidades

● Hardware estático. Se refiere al conjunto de elementos materiales de sistemas electronicos.

● Hardware reconfigurable. Es aquél que es descrito mediante un
HDL (Hardware Description Language). Se desarrolla de manera
similar a como se hace software.

El ejemplo tipico en este caso es la placa Arduino

**Plataformas Open Source**

● GitLab

● SourceForge

● Cloud Source Repositories

● GitKraken

● Apache Allura


## Links de interes

**[Listado de Proyectos Open Hardware](https://en.wikipedia.org/wiki/List_of_open-source_hardware_projects)**
