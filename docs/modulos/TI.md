---
hide:
    - toc
---

# Taller Integrador presencial


Durante este taller se nos presento el desafio de intervenir una impresora 3D Anete 8A, con un extrusor de pasta, de forma de que pudieramos mprimir con materiales biodegradables, al final de la jornada de 3 dias se realizo una presentacion con los resultados optenidos por los diferentes grupos.

Las actividades se ejecutaron de la siguiente forma:

**Jueves 31/03**

●	Laboratorios de Co-creación Armado de equipos de trabajo  (3) / división de tareas

![](../images/TI/3.png){ width=50% }

●	Preparación Marlin Anet A8 / programación

![](../images/TI/5.png){ width=50% }

●	Montaje extrusor de pasta

![](../images/TI/6.png){ width=50% }

●	Pruebas de impresión en pasta

![](../images/TI/1.png){ width=50% }

●	Impresión 3d fdm (piezas para mejorar extrusor)

<iframe width="320" height="539" src="https://www.youtube.com/embed/s5rdEsSPDt8" title="IMG 2143" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**Viernes 01/04**

●	Montaje de piezas impresas en fdm

![](../images/TI/7.png){ width=50% }

![](../images/TI/8.png){ width=50% }

●	Preparación de pasta (probar tres opciones)

![](../images/TI/9.png){ width=50% }

●	Modelado 3d de pieza a imprimir en pasta

<iframe width="320" height="539" src="https://www.youtube.com/embed/FHGnSLwM8UQ" title="IMG 2134" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

●	Experimentación

**Sabado 01/04**

●	Soldado de Placas diseñadas en el Modulo Tecnico 08

![](../images/TI/10.png){ width=50% }

![](../images/MT08/3.png){ width=50% }

![](../images/MT08/2.jpeg){ width=50% }

<iframe width="560" height="315" src="https://www.youtube.com/embed/zh7JhqCbUSI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Referencias:**

**[Impresora Anet A8](https://anet3d.com/)**

**[Anet board en Arduino IDE](https://miro.com/app/board/o9J_lxIOIrc=/)**

**[Codigo Anet board en Arduino IDE](https://github.com/SkyNet3D/anet-board)**

**[MarlinFirmware](https://github.com/MarlinFirmware/Marlin)**
