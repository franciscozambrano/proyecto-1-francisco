---
hide:
    - toc
---
# MI - Emprendimiento

Las actividades de este modulo se orientaron principalmente a las herramientas necesarias para materializar las ideas de negocio originadas de nuestros proyectos finales.
![](../images/MI01/EyN1.jpeg){ width=70% }
Una de las variables más importantes y complejas es la definición de los miembros de un equipo de trabajo y el rol que estos representan en una organización, en este sentido se plateo la herramienta del diagrama de belbin.

![](../images/MI01/rolesBelbin.jpg){ width=70% }

Otro de los enfoques abarcados durante este módulo fue el Human Centered Design, que en pocas palabras se basa en empatizar con los usuarios de tu producto y orientar el diseño a satisfacer sus necesidades

![](../images/MI01/HCD.png){ width=70% }

Finalmente como entrega final de este modulo se nos pidio desarrollar una canvas y un diagrama de Gantt de nestro posible proyecto final. Mediante la utilizacion de estas herramientas se procuro visibilizar el modelo de negocio de un proyecto propio, en mi caso particular el diseño de un hexoesqueleto que ofrezca una solución ergonómica, práctica y robótica para las articulaciones de la mano que mejore la calidad de vida de adultos mayores.


[Canvas del Proyecto](../files/Canvas.pdf)

[Cronograma de Actividades](../files/Gantt.pdf)
