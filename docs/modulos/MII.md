---
hide:
    - toc
---

# MI 02 - Diseño Computacional


El diseño computacional es la aplicación de estrategias de cálculo en el proceso de diseño. Mientras que tradicionalmente los diseñadores se basan en la intuición y en la experiencia para resolver problemas de diseño, el diseño computacional tiene como objetivo mejorar dicho proceso mediante la codificación de las decisiones de diseño utilizando un lenguaje de programación. El objetivo no es documentar el resultado final necesariamente, sino más bien los pasos necesarios para crear ese resultado.

La mayoría de los entornos de diseño computacional dependen de la programación visual, en contraposición a la programación basada en el texto tradicional. Con la programación visual, los programas se arman gráficamente en lugar de con código escrito.
Hay una serie de herramientas de diseño computacional en el mercado. La mayoría de estas herramientas funcionan al margen de otras plataformas de software, como Microstation, Rhino o Revit. He aquí un desglose de las cinco herramientas más populares de diseño informático. Dynamo es la herramienta de programación visual de Autodesk, pero la mas popular y la usada durante este módulo fue Grasshopper una herramienta de modelado algorítmico de Rhino, un software de modelado 3D desarrollado por Robert McNeel & Associates. El docente encargado de este modulo fue **[Arturo de la Fuente]( https://www.arturodelafuente.com)**, quien posee una larga experiencia en el uso de Grasshopper y Rhino.

La variación de los parámetros de diseño es lo que obtenemos al usar algoritmos computacionales lo que otorga la capacidad de personalizar las variables, esto lo veo muy útil en el desarrollo de un proyecto como el mío.

![](../images/MI02/DP.png){ width=50% }

La experiencia con Rhino y Grasshopper no fue nada sencilla, el potencial de la herramienta va atado a la inmensa variedad de aplicaciones y opciones que tiene lo requiere una amplia documentación, para empezar creo que la aplicación en mi proyecto seria en el diseño del guantin que sostendría los mecanismos del exoesqueleto, mi idea no es que sea un guante completo, si no como una malla sobre la mano que cubra solo lo necesario, para sostener y darle funcionalidad a la estructura. Para empezar escoji el problema hecho por de las columnas construidas sobre una curva y varie la orientacion de las columnas, su diametro y la curva base hice bake en varias iteracciones para familiarizarme con la herramienta.

![](../images/MI02/SS5.png)
![](../images/MI02/ss4.png)

Como se ve en la figura el primer intento no ha sido exitoso, falta sobre todo tener claro cuáles son las líneas de generación del volumen del guante, por eso para hacerlo mas sencillo y practicar con la herramienta me enfoque solo en los dedos y alrededor de ellos genero un voronoi sobre la superficie de los cilindros, luego aplique el ad-ons de Grasshopper **[Weaverbird](https://grasshopperdocs.com/addons/weaverbird.html)** que es un un modelador topológico este complemento reconstruye la forma, subdivide cualquier malla, incluso hecha por polilíneas esto para cerrar las formas que no cuadraban. lo cual hasta horan he logrado.

![](../images/MI02/SS1.png)
![](../images/MI02/SS2.png)
![](../images/MI02/SS3.png)


## Links de interes

**[Grasshopper](http://grasshopperprimer.com/en/index.html)**
**[Food 4 Rhino](https://www.food4rhino.com/en)**
**[Shapediver](https://shapediver.com/)**
**[Rhinoceros Forums](https://shapediver.com/)**
**[Weaverbird – Topological Mesh Editor](https://www.giuliopiacentino.com/weaverbird/)**
**[Nervous System](https://n-e-r-v-o-u-s.com/)**
**[GrasshopperDocs](http://grasshopperdocs.com/)**
**[Gediminas Kirdeikis YouTube Channel](https://www.youtube.com/c/GediminasKirdeikis)**
**[Thinkparametric](https://thinkparametric.com/library)**



## Apps
**[Mani](https://www.food4rhino.com/en/app/manis)** **[wasp](https://www.food4rhino.com/en/app/wasp)** **[Crane](https://www.food4rhino.com/en/app/crane)** **[Biomorphere](https://www.food4rhino.com/en/app/biomorpher)** **[Ameba](https://www.food4rhino.com/en/app/ameba)** **[Wallacei](https://www.food4rhino.com/en/app/wallacei)** **[Cocoon](https://grasshopperdocs.com/components/cocoon/cocoon.html)** **[Flexhopper](https://www.food4rhino.com/en/app/flexhopper)** **[Shortest Walk](https://www.food4rhino.com/en/app/shortest-walk-gh)** **[Heron](https://www.food4rhino.com/en/app/heron)** **[Bison](https://www.food4rhino.com/en/app/bison)**





##Charla Inteligencia Artifical Colectiva
<iframe width="560" height="315" src="https://www.youtube.com/embed/LHgVR0lzFJc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
