---
hide:
    - toc
---


# Idea y Concepto

La idea del proyecto es ofrecer una solución ergonómica, práctica y robótica para las articulaciones de la mano que mejore la calidad de vida de adultos mayores o personas con dificultades de movilidad en los miembros superiores.
