---
hide:
    - toc
---

# Desarrollo
Inicialmente pesamos diseñar completamente la articulación de la mano sin embrago notamos que existen ya múltiples propuestas de los mismos por lo que para agilizar el desarrollo del proyecto seleccionamos un sistema basado en cables y actuadores lineales y lo adaptamos a uno con un servomotor y un sistema rueda cremallera

![](../images/MT04/Conjunto.png){ width=50% }

Intento inicial para el diseño de las articulaciones

![](../images/PF/10.png){ width=50% }

Luego se procedió a eliminar del diseño seleccionado la ultima falange para poder adaptar los torillos de acople de los cables

![](../images/PF/10-1.png){ width=50% }

![](../images/PF/11.png){ width=50% } ![](../images/PF/12.png){ width=50% }

Luego pasamos a adaptar e imprimir el sistema de rueda y cremallera que iría acoplado al servomotor, en este caso archivo stl se encontraba disponible en la web thingerverse y solo se adaptó a las dimensiones requeridas para para imprimirlo en la Ultimaker

![](../images/PF/1.png){ width=50% }

Y luego de su impresion a su ensamblae con el servomotores

![](../images/PF/2.png){ width=50% }

Una vez completada esta etapa se procedió a programar el movimiento de la cremallera con Arduino y se utilizo para su control un potenciómetro, el procedimiento se diagramó y simuló en Autodesk Tinkercad y se programó con el Arduino IDE

![](../images/PF/3.png){ width=50% }

![](../images/PF/3-1.png){ width=50% }

Una vez comprobado el funcionamiento de prototipo con el Arduino conectado a los elementos de control e impulso mediante una Protoboard, se exporto el circuito a Autodesk Eagle para el diseño y fabricación de una PCB para conectarla a la placa Arduino

![](../images/PF/4.png){ width=50% }

<iframe width="560" height="315" src="https://www.youtube.com/embed/p790FiOCcrc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/WiFc_w-onhs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Con la PCB cortada se procedió a soldar el potenciómetro y los pines de conexión con el servomotor y la placa Arduino

![](../images/PF/6.png){ width=50% }
![](../images/PF/7.png){ width=50% }
![](../images/PF/8.png){ width=50% }

Al El modelo de las articulaciones  sobre tela tipo Tul se le adapto en el espacio destinado para ello tela tipo velcro de forma que luego pueda este sistema acoplarse a los dedos y muñeca del usuario.



![](../images/PF/13.png){ width=50% }
![](../images/PF/14.png){ width=50% }
![](../images/PF/15.png){ width=50% }

Luego se colocaron los cables y guías en cada uno de los dedos y se conecto uniones los cables con la cremallera, para proceder a probar el conjunto

![](../images/PF/16.png){ width=50% }
![](../images/PF/17.png){ width=50% }

**[Conjunto en Funcionamiento](https://youtube.com/shorts/1uyP-T8NhD8?feature=share)**
