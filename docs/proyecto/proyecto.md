---
hide:
    - toc
---

# Propuesta final

**¿Qué hace?**

La idea es diseñar y construir un dispositivo que facilite el movimiento de la mano en aquellas personas que, por diversas causas, tienen dificultades para ello. El mecanismo impulsara el abrir y cerrar los dedos de la mano de forma que les sea más fácil asir y soltar objetos cotidianos, que usualmente no pueden utilizar.

**¿Cómo llegaste a la idea?**

La idea parte del aumento de personas con este tipo de patologías, sobre todo en la franja etaria de los adultos mayores quienes con el aumento de la expectativa de vida tienden a presentar esta problemática sin que sea necesariamente producto de accidentes o malformaciones.

**Motivación**

La motivación principal ha sido el estar en contacto estrecho con personas que presentan esta dificultad y también el haber estado involucrado en el en unidades curriculares de la Carrera de Ingeniería Mecatrónica que abarcan esta temática.

**¿Quién lo ha hecho de antemano?**

Existen muchos modelos y prototipos orientados en esta problemática, desde diseños altamente sofisticados (la mayoría de estos se caracterizan por su tamaño y alto costo) hasta los más sencillos que buscan simplemente rehabilitar a pacientes con alguna lesión temporal por lo cual la comodidad o apariencia no es una limitante, mi idea era conseguir un equilibrio entre ambas vertientes.

**¿Qué diseñaste?**

Habiendo intentado inicialmente diseñara un mecanismo que simulara las falanges de los dedos abriendo (o cerrándolos) mediante un conjunto de barras y pasadores, opte finalmente por un mecanismo impulsado por cables impreso sobre una tela de tull, el diseño original (mas complejo que el logrado en este proyecto) está en la web **[thingiverse](https://www.thingiverse.com/thing:4819739)** , en este caso alteramos el dispositivo de agarre de los cables en los dedos y en vez de actuadores los cuales son muy grandes en el sistema original se sustituyo por un sistema de rueda dentada y cremallera impulsadas por un servomotor, también se diseñó en el circuito para controlar el servomotor con un potenciómetro, mediante una placa Arduino UNO, al cual se le adapto una placa diseñada en Eagle  y cortada para tal fin.

**¿Qué materiales y componentes se utilizaron?**

Los materiales utilizados fueron, Material pla 3.0 para la impresora 3d ultimaker, tela tipo tull, cables (con sus cubiertas) para unir las articulaciones, sujetadores de para el cable (atornillados a la última falange, Arduino UNO, potenciómetro y el servomotor modelo MG995, tela tipo tull.

**¿Qué partes y sistemas se fabricaron?**

Se diseño la conexión de los cables con, la última “falange” y se fabricó junto con todas las demás articulaciones imprimiéndola sobre tela tipo tull en la impresora 3 D, igualmente se fabricaron la rueda y la cremallera, también se diseñó  fabricó la placa con el circuito que se acoplo al Arduino para controlar el servomotor mediante el potenciómetro.

**¿Qué procesos se utilizaron?**

Los procesos diseños y adecuación de modelos existentes en 2D y 3D se realizaron en el Fusión 360, y para la impresión de los mismos el Cura, para la simulación del circuito se utilizó el simulador de tinkercad, y con el Eagle se diseñó el circuito definitivo, y para la programación de la placa el Arduino IDE.

Se utilizaron procesos aditivos en la impresión de las articulaciones y el sistema impulsor y en la soldadura del potenciómetro a la placa  y sustractivos en el milling de la PCB.

**¿Qué preguntas se respondieron?**

La principal interrogante que se aclaro fue que efectivamente un sistema controlado por un simple servomotor y un cable puede servir para contraer y sobre todo extender, las articulaciones en nuestro caso solo lo comprobamos para un dedo pero entendemos que es perfectamente replicable para todos los dedos de la mano, también quedo claro que el sistema de sujeción de lasa articulaciones debería imprimirse sobre un material más resistente que la tela o que la tela quedara dentro de la impresión y no bajo esta ya que al trabajar en el ensamblado es bastante fácil que se desprendan las pequeñas piezas.

**¿Qué funcionó? ¿Qué no?**

Lo que funciono perfectamente fue el sistema impulsor de la articulación lo que queda por mejorar es la robustes de los componentes ya que como señalamos la impresión sobre tela le da demasiada fragilidad al sistema ya desde el ensamblaje, más aún si se considera el uso del dispositivo en actividades cotidianas probablemente tenga una vida útil muy corta.

**¿Cuáles son los pasos a seguir?**

Principalmente, continuar con el diseño de la muñequera donde debería ir ajustado el sistema impulsor (rueda, cremallera y servomotor), para esto pensamos probar con dispositivos más pequeños que permitan simplificar el diseño y tamaño de la misma, luego quedaría probar  imprimir no sobre tela sino sobre una cama de material flexible tipo **[TPE](https://www.impresoras3d.com/imprimir-3d-con-materiales-flexibles/)** o **[TPU](https://www.3dnatives.com/es/guia-completa-tpu-040620202/)** igualmente probar con otro diseño de articulación que soporte mayores cargas dinámicas sobre todo en el proceso de apertura que es donde el sistema de cables puede llegar a fallar



**Conclusiones**

Durante el desarrollo del proyecto se utilizaron procesos aditivos como el de diseño e impresión 3D, diseño y corte de placas de circuitos eléctricos y la programación de la placa Arduino para el control de los dispositivos electrónicos.
La operación de las falanges de los dedos funcióno bastante bien con los cables gracias a las guías lo que permite el cierre de los de dedos sin que se comprima el cable en todo caso falto el diseño e impresión de una base para la cremallera y el servomotor para un funcionamiento mas independiente del mecanismo de extensión y retracción de los dedos.
Una de las principales dificultades que se encontró en el ensamblaje final fue lo delicado de la impresión sobre tela loque origina la inquietud de utilizar otros materialles de impresión (como los mensionados anteriormente) que le de mas robustez al conjunto y haga mas fácil su ensamblado

![](../images/PF/Slide3.png){ width=50% }


<iframe width="560" height="315" src="https://www.youtube.com/embed/fyeAdo6AcXI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Archivos Editables del Proyecto**

[Codigo Arduino Engranaje](../images/PF/SCP.ino)
[Archivos STL Engranajes](../images/PF/Eng.rar)
[Archivos STL Articulaciones](../images/PF/Mano.STL)
[Archivo brd de la PCB](../images/MT08/SCP.brd)
