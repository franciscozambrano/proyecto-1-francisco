#include <Servo.h>
 
//Para el potenciometro
 
const int potenciometro=A0;
int valor; //variable que almacena la lectura analógica raw
int valorpoten; //posicion del potenciometro en tanto por ciento
 
 
//Para el HC-SR04
int trigPin = 13; //por donde enviamos el sonido
int echoPin = 12; //por donde recibimos el sonido
 
//Para el servomotor
 
Servo motor;
int pinmotor = 9;
const int angulomov = 55;
void setup() {
  
 Serial.begin(9600);
 motor.attach(pinmotor);
 pinMode(trigPin, OUTPUT);
 pinMode(echoPin, INPUT);
  
}
 
void loop() {
  
 //Leemos la configuracin del potencimetro para saber el valor de distancia que se ha configurado
 valor = analogRead(potenciometro);
 //Pasamos el valor a centmetros de 0 a 10
 valorpoten = map(valor, 0, 1023, 0, 10); 
 //Leemos la distancia desdel HC-SR04
  
 long duracion, distancia ;
  
 digitalWrite(trigPin, LOW); // Nos aseguramos de que el trigger está desactivado
 delayMicroseconds(2); // Para estar seguros de que el trigger esta LOW
 digitalWrite(trigPin, HIGH); // Activamos el pulso de salida
 delayMicroseconds(10); // Esperamos 10µs. El pulso sigue active este tiempo
 digitalWrite(trigPin, LOW); // Cortamos el pulso y a esperar el echo
 
 duracion = pulseIn(echoPin, HIGH) ;
 distancia = duracion / 2 / 29.1 ;
  
  
 //Movemos el servo si estamos dentro de la distancia marcada por el potenciometro
 if (distancia < valorpoten){
          motor.write(angulomov);
    }
 else
   {
           motor.write(10);
   } 
 delay(300);
  
 //Imprimimos los valores por el terminal 
 Serial.print("Valor potenciometro ");
 Serial.println(valorpoten);
 Serial.print("Distancia ");
 Serial.println(distancia);
  
  
 
}
