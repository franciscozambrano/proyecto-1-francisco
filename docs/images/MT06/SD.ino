int pin_boton = 4;
bool estado = LOW;

void setup() {
    pinMode (pin_boton, INPUT);
    Serial.begin(115200);
}

void loop() {
    // Leemos el nuevo estado de el botón
    bool estado_nuevo = digitalRead(pin_boton);
    
    // Si el estado cambió lo publicamos
    if (estado != estado_nuevo) {
        estado = estado_nuevo;
        Serial.println(estado);
    }
    
    delay(50);
}
