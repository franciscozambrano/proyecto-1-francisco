# Sobre mi

![](../images/IO1.jpg)

<p align="justify">
Ingeniero Mecánico con más de 10 años de experiencia en el diseño de tuberías a presión y sistemas de bombeo en sistemas de distribución. Experiencia en el diseño de sistemas de aire acondicionado, mantenimiento preventivo por medio de análisis de vibraciones y velocidades críticas en equipos rotativos, desarrollo de alcances e ingeniería de detalle de proyectos y preparación de la documentación que sirva de base para contratación de la ejecución del mismo, estimación de cómputos métricos y establecimiento las especificaciones técnicas que regirán las características del producto. Más de 15 años de experiencia como docente universitario en el área de Física, Matemáticas, Mecánica aplicada y Mecánica de los Fluidos. Asesor y tutor de proyectos de investigación académica y tesis de grado.
</p>

**[my website](https://utec.edu.uy/es/educacion/carrera/ingenieria-en-mecatronica/)**
